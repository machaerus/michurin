package main

import (
	"math/rand"
	"time"
)

type Sensor struct {
	Temperature float64
	Humidity float64
	Moisture float64
	MoistureDecayRate float64
}

func (s *Sensor) Water() {
	s.Moisture = 100.0
}

func (s *Sensor) Update() (float64, float64, float64) {
	rand.Seed(time.Now().UnixNano())

	newTemperature := 20 + rand.Float64() * 5
	newHumidity := 50 + rand.Float64() * 30
	// Moisture calculations
	actualDecay := (s.MoistureDecayRate * s.Temperature * 100.0) /
		(s.Humidity * s.Moisture) + 0.03
	newMoisture := s.Moisture - rand.Float64() * actualDecay

	s.Temperature = newTemperature
	s.Humidity = newHumidity
	s.Moisture = newMoisture

	return newTemperature, newHumidity, newMoisture
}
