package main

import (
	"time"
	"fmt"
	"log"
	"os"
	"encoding/json"
	"strings"
	"os/signal"
	"syscall"

	"github.com/eclipse/paho.mqtt.golang"
	"github.com/joho/godotenv"
)

const CLIENT_ID string = "Simulator"
const PLANT_ID string = "BIOLLANTE"
const BASE_TOPIC string = "michurin"
const SENSOR_TOPIC string = "sensor"
const COMMAND_TOPIC string = "command"
const INGESTION_TARGET_ID string = "IngestionService"

func simulate(sensor *Sensor, mqttClient mqtt.Client) {
	for {
		payload := Payload{
			Timestamp: time.Now().UTC(),
			SenderID: CLIENT_ID,
			TargetID: INGESTION_TARGET_ID,
			PlantID: PLANT_ID,
			MType: SENSOR_TOPIC,
			Data: []Datapoint{
				{ Name: "moisture", Value: sensor.Moisture },
				{ Name: "humidity", Value: sensor.Humidity },
				{ Name: "temperature", Value: sensor.Temperature }}}

		fmt.Println(payload)
		text, err := json.Marshal(payload)
		if err != nil {
			panic(err)
		}
		topic := BASE_TOPIC+"/"+SENSOR_TOPIC+"/"+CLIENT_ID+"/"+PLANT_ID
		token := mqttClient.Publish(topic, 0, false, text)
		token.Wait()

		// Step forward
		sensor.Update()
		time.Sleep(5 * time.Second)
	}
}

func parsePayload(msg mqtt.Message) *Payload {
	var payload Payload
	err := json.Unmarshal(msg.Payload(), &payload)
	if (err != nil) {
	   fmt.Println(err)
	}
	return &payload
}

func handleMessage(payload *Payload, sensor *Sensor) {
	if payload.MType == COMMAND_TOPIC {
		for _, datapoint := range payload.Data {
			if datapoint.Name == "water" {
				sensor.Water()
			}
		}
	}
}

func topicSubscribe(mqttClient mqtt.Client, topic string) {
	if token := mqttClient.Subscribe(topic, 0, nil);
			token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}
	// Create an async interrupt handler with tear-down logic.
	// This is for gentle Ctrl+C shutdown.
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func(mqttClient mqtt.Client) {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		if token := mqttClient.Unsubscribe(topic);
				token.Wait() && token.Error() != nil {
			fmt.Println(token.Error())
			os.Exit(1)
		}
		mqttClient.Disconnect(250)
		time.Sleep(1 * time.Second)
		fmt.Println("Done with waiting, unsubscribing!")
		os.Exit(0)
	}(mqttClient)
	fmt.Println("Waiting for incoming messages!")
}

func getMQTTClient(
	brokerURL string, clientID string,
	username string, password string) (mqtt.Client, chan *Payload) {
	// Setup the incoming message queue and the message handler.
	messageQueue := make(chan *Payload)
	var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
		// This handler only logs the incoming message and adds it
		// to the queue. Every such message will be then consumed
		// by a separate goroutine.
		log.Println("New message!")
		log.Printf("TOPIC: %s MESSAGE: %s\n", msg.Topic(), msg.Payload())
		// Validate topic.
		topic := strings.Split(string(msg.Topic()), "/")
		if !(
			topic[0] == BASE_TOPIC &&
			(topic[1] == SENSOR_TOPIC || topic[1] == COMMAND_TOPIC)) {
			log.Println("Invalid topic! Discarding message.")
			return
		}
		// TODO: validate more stuff!
		//
		// Parse and validate the message.
		if payload := parsePayload(msg); payload != nil {
			messageQueue <- payload
		} else {
			log.Println("Incorrect payload detected and discarded!")
		}
	}
	// Setup the client.
	// mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)
	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
	opts := mqtt.NewClientOptions().
		AddBroker(brokerURL).
		SetClientID(clientID)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetDefaultPublishHandler(f)
	opts.SetPingTimeout(1 * time.Second)

	opts.SetUsername(username)
	opts.SetPassword(password)

	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return client, messageQueue
}

// func getMQTTClient(brokerURL string, clientID string) (mqtt.Client, chan Payload) {
// 	// Setup the incoming message queue and the message handler.
// 	incomingQ := make(chan Payload)
// 	var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
// 		// This handler only logs the incoming message and adds it
// 		// to the queue. Every such message will be then consumed
// 		// by a separate goroutine.
// 		fmt.Println("New payload arrived! Adding to the incoming queue.")
// 		fmt.Printf("TOPIC: %s\n", msg.Topic())
// 		fmt.Printf("MSG: %s\n", msg.Payload())
// 		// Parse the message
// 		payload := parsePayload(msg)
// 		incomingQ <- payload
// 	}
// 	// Setup the client.
// 	// mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)
// 	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
// 	opts := mqtt.NewClientOptions().
// 		AddBroker(brokerURL).
// 		SetClientID(clientID)
// 	opts.SetKeepAlive(2 * time.Second)
// 	opts.SetDefaultPublishHandler(f)
// 	opts.SetPingTimeout(1 * time.Second)

// 	opts.SetUsername(username)
// 	opts.SetPassword(password)

// 	client := mqtt.NewClient(opts)
// 	if token := client.Connect(); token.Wait() && token.Error() != nil {
// 		panic(token.Error())
// 	}
// 	return client, incomingQ
// }

func main() {
	fmt.Println("Load env variables")
	err := godotenv.Load("local.env")
	if err != nil {
		log.Fatalf("Fatal error occured while loading env: %s", err)
	}
	var BROKER_URL string = os.Getenv("BROKER_URL")
	var BROKER_USER string = os.Getenv("BROKER_USER")
	var BROKER_PASS string = os.Getenv("BROKER_PASS")

	fmt.Println("Setting up MQTT client")
	mqttClient, q := getMQTTClient(
		BROKER_URL, CLIENT_ID, BROKER_USER, BROKER_PASS)
	cmdTopic := BASE_TOPIC+"/"+COMMAND_TOPIC+"/"+CLIENT_ID+"/"+PLANT_ID
	topicSubscribe(mqttClient, cmdTopic)

	fmt.Println("Initializing sensor")
	sensor := Sensor{24.0, 60.0, 100.0, 1.0}

	fmt.Println("Starting the simulator")
	go simulate(&sensor, mqttClient)

	fmt.Println("Starting the command listener")
	for {
		payload := <-q
		go handleMessage(payload, &sensor)
	}
	// FIXME: The sensor is not thread safe!
}
