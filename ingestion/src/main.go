package main

import (
	"log"
	"os"
	"time"

	"github.com/eclipse/paho.mqtt.golang"
)

const CLIENT_ID string = "IngestionService"
const BASE_TOPIC string = "michurin"
const SENSOR_TOPIC string = "sensor"
const COMMAND_TOPIC string = "command"

func plantNeedsWater(
	plantID string,
	timestamp time.Time,
	moisture float64) bool {
	// Determine if the datapoint is not too old to take action (is this
	// a necessary measure, can this ever happen that data would be old?)
	//
	threshold := 0.25
	log.Printf("Testing plant %s moisture: %d against %d threshold.",
		plantID, moisture, threshold)
	return moisture < threshold
}

// Process sensor data payload and take action if appropriate.
func processPayload(payload *Payload, mqttClient mqtt.Client) {
	// All the analytics go here. Create events and issue commands.
	// E.g. if moisture is too low, issue a command to the IoT node
	// to run the water pump.
	// Check values against criteria. Send watering command if the moisture
	// level is too low.
	//
	// Currently the criteria are generic and hardcoded, but it is fairly
	// easy now to keep specific, configurable criteria in the database
	// for all plants separately, and fetch it from there.

	// How long should the pump be working (in seconds).
	// This can be later customized per-plant, or made dynamic based on
	// environmental conditions.
	waterSecs := 5.0
	for _, datapoint := range payload.Data {
		if datapoint.Name == "moisture" {
			moisture := datapoint.Value
			if plantNeedsWater(payload.PlantID, payload.Timestamp, moisture) {
				log.Println("Plant needs water! Issuing water command.")
				sendCommand(
					"water",
					waterSecs,
					payload.SenderID,
					payload.PlantID,
					mqttClient)
			}
			break
		}
	}
}

// Handle an incoming message payload.
func handleMessage(payload *Payload, dbc DBClientConfig, mqttClient mqtt.Client) {
	log.Println("Writing payload to the database.")
	storeData(dbc, payload)
	switch payload.MType {
	case SENSOR_TOPIC:
		log.Println("Received sensor data. Processing.")
		processPayload(payload, mqttClient)
	case COMMAND_TOPIC:
		log.Println("Received a command.")
	default:
		log.Println("Invalid message type!")
	}
}

func main() {
	log.Println("Load env variables")
	// The env variables should be loaded by systemd now!
	// err := godotenv.Load("../local.env")
	// if err != nil {
	// 	log.Fatalf("Fatal error occured while loading env: %s\n", err)
	// }
	var INFLUX_URL string = os.Getenv("INFLUX_URL")
	var INFLUX_TOKEN string = os.Getenv("INFLUX_TOKEN")
	var INFLUX_ORG string = os.Getenv("INFLUX_ORG")
	var INFLUX_BUCKET string = os.Getenv("INFLUX_BUCKET")
	var BROKER_URL string = os.Getenv("BROKER_URL")
	var BROKER_USER string = os.Getenv("BROKER_USER")
	var BROKER_PASS string = os.Getenv("BROKER_PASS")

	log.Printf("INFLUX_URL = %s", INFLUX_URL)
	log.Printf("INFLUX_TOKEN = %s", INFLUX_TOKEN)
	log.Printf("INFLUX_ORG = %s", INFLUX_ORG)
	log.Printf("INFLUX_BUCKET = %s", INFLUX_BUCKET)
	log.Printf("BROKER_URL = %s", BROKER_URL)
	log.Printf("BROKER_USER = %s", BROKER_USER)
	log.Printf("BROKER_PASS = %s", BROKER_PASS)
	log.Printf("CLIENT_ID = %s", CLIENT_ID)

	// Configuration for the InfluxDB client.
	dbc := DBClientConfig{
		INFLUX_URL, INFLUX_TOKEN, INFLUX_ORG, INFLUX_BUCKET}

	// Create the MQQT client and connect to the broker.
	mqttClient, q := getMQTTClient(
		BROKER_URL, CLIENT_ID, BROKER_USER, BROKER_PASS)
	// Start listening.
	// We are subscribed to all subtopics, because we want to store it
	// all in the database. This can be made more sophisticated in
	// the future.
	subscribeToTopic(mqttClient, BASE_TOPIC+"/#")
	for {
		// To make this more safe and scalable, there should be a limit
		// to how many handlers can run at the same time. For now I'm
		// keeping it simple because there are relatively very few
		// incoming messages expected.
		payload := <-q
		go handleMessage(payload, dbc, mqttClient)
	}
}
