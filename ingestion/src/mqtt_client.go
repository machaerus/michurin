package main

import (
	"encoding/json"
	"os"
	"log"
	"strings"
	"time"
	"os/signal"
	"syscall"
	"github.com/eclipse/paho.mqtt.golang"
)

func parsePayload(msg mqtt.Message) *Payload {
	var payload Payload
	err := json.Unmarshal(msg.Payload(), &payload)
	if (err != nil) {
		log.Println("Incorrect payload structure!")
		log.Println(err)
	}
	if payload.SenderID == "" ||
		payload.TargetID == "" ||
		payload.PlantID == "" ||
		payload.MType == "" {
		return nil
	}
	log.Println(payload)
	return &payload
}

func getMQTTClient(
	brokerURL string, clientID string,
	username string, password string) (mqtt.Client, chan *Payload) {
	// Setup the incoming message queue and the message handler.
	messageQueue := make(chan *Payload)
	var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
		// This handler only logs the incoming message and adds it
		// to the queue. Every such message will be then consumed
		// by a separate goroutine.
		log.Println("New message!")
		log.Printf("TOPIC: %s MESSAGE: %s\n", msg.Topic(), msg.Payload())
		// Validate topic.
		topic := strings.Split(string(msg.Topic()), "/")
		if !(
			topic[0] == BASE_TOPIC &&
			(topic[1] == SENSOR_TOPIC || topic[1] == COMMAND_TOPIC)) {
			log.Println("Invalid topic! Discarding message.")
			return
		}
		// TODO: validate more stuff!
		//
		// Parse and validate the message.
		if payload := parsePayload(msg); payload != nil {
			messageQueue <- payload
		} else {
			log.Println("Incorrect payload detected and discarded!")
		}
	}
	// Setup the client.
	// mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)
	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
	opts := mqtt.NewClientOptions().
		AddBroker(brokerURL).
		SetClientID(clientID)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetDefaultPublishHandler(f)
	opts.SetPingTimeout(1 * time.Second)

	opts.SetUsername(username)
	opts.SetPassword(password)

	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return client, messageQueue
}

func subscribeToTopic(mqttClient mqtt.Client, topic string) {
	if token := mqttClient.Subscribe(topic, 0, nil);
			token.Wait() && token.Error() != nil {
		log.Println(token.Error())
		os.Exit(1)
	}
	// Create an async interrupt handler with tear-down logic.
	// This is for gentle Ctrl+C shutdown.
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func(mqttClient mqtt.Client) {
		<-c
		log.Println("\r- Ctrl+C pressed in Terminal")
		if token := mqttClient.Unsubscribe(topic);
				token.Wait() && token.Error() != nil {
			log.Println(token.Error())
			os.Exit(1)
		}
		mqttClient.Disconnect(250)
		time.Sleep(1 * time.Second)
		log.Println("Done with waiting, unsubscribing!")
		os.Exit(0)
	}(mqttClient)
	log.Println("Waiting for incoming messages...")
}

func sendCommand(
	name string,
	value float64,
	targetID string,
	plantID string,
	mqttClient mqtt.Client) {

	command := Payload{
		time.Now().UTC(),
		CLIENT_ID,
		targetID,
		plantID,
		COMMAND_TOPIC,
		[]Datapoint{Datapoint{name, value}}}

	text, err := json.Marshal(command)
	if err != nil {
		panic(err)
	}

	token := mqttClient.Publish(
		BASE_TOPIC+"/"+COMMAND_TOPIC+"/"+targetID+"/"+plantID,
		0, false, text)
	token.Wait()
}
