package main

import "time"

type DBClientConfig struct {
	URL 		string
	Token 		string
	Org 		string
	Bucket 		string
}

type Datapoint struct {
	Name  		string		`json:"name"`
	Value 		float64		`json:"value"`
}

type Payload struct {
	Timestamp 	time.Time	`json:"timestamp"`
	SenderID  	string		`json:"senderId"`
	TargetID  	string		`json:"targetId"`
	PlantID  	string		`json:"plantId"`
	MType  		string		`json:"mType"`
	Data      	[]Datapoint `json:"data"`
}
