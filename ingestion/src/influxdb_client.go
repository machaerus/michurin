package main

import (
	"github.com/influxdata/influxdb-client-go/v2"
	"log"
)

func storeData(dbc DBClientConfig, payload *Payload) {
	// https://docs.influxdata.com/influxdb/cloud/api-guide/client-libraries/go/
	// and InfluxDB in-app tutorial

	// Create a database client
	client := influxdb2.NewClient(dbc.URL, dbc.Token)
	defer client.Close()
	// Get non-blocking write client
	writeAPI := client.WriteAPI(dbc.Org, dbc.Bucket)
	// Create point using fluent style
	p := influxdb2.NewPointWithMeasurement(payload.MType).
		AddTag("senderId", payload.SenderID).
		AddTag("targetId", payload.TargetID).
		AddTag("plantId", payload.PlantID).
		SetTime(payload.Timestamp)
	// iteratively add fields to the payload
	for _, datapoint := range payload.Data {
		p = p.AddField(datapoint.Name, datapoint.Value)
	}
	log.Printf("Storing new point: %+v", p)
	// write point asynchronously
	writeAPI.WritePoint(p)
	// Flush writes
	writeAPI.Flush()
}
