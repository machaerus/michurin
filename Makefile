.PHONY: help
help:
	@echo "See Makefile for available make commands."

.PHONY: readme
readme:
	pandoc -s README.org -o README.md

.PHONY: broker
broker:
	mosquitto

.PHONY: database
database:
	influxd

.PHONY: pub
pub:
	mosquitto_pub -t 'michurin/sensors' -f ingestion/payload.json

.PHONY: sub
sub:
	mosquitto_sub -t 'michurin/#' -v
