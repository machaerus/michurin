#+TITLE: Project Michurin
#+AUTHOR: Jan Szwagierczak
#+OPTIONS: H:5

* Overview

New version of Project Michurin: IoT-based house plant management platform/solution.

* Architecture
** Server
*** MQTT broker
Mosquitto broker running as a service (locally).
*** Ingestion
Go service which:
- Consumes sensor data from the broker.
- Logs the data and pushes it to the database.
- Simultaneously analyses the incoming data and decides if an action should be taken.
- Depending on the decision, creates events to be logged in the database, and/or publishes commands to the broker, to be passed to the relevant IoT device.
*** Database
InfluxDB to keep the sensor data and events produced by the ingestion or the user. Running as a service (locally).
*** User API
Users can use the API to request sensor data from the database, as well as to push commands to a chosen device.
*** Dashboard
Simple data visualisation dashboard, Julia + Genie/Stipple (for example).

** IoT nodes
Arduino devices with Wifi/GSM capability and an array of sensors for measuring soil moisture level, air temperature and air humidity.

Each device is additionally connected to a water pump which can be activated directly by a sensor event (offline mode), or by a command from the server (online mode). Sending data and receiving commands is done via MQTT protocol.

** Clients
Desktop client for displaying data or notifications in terminal, status bar, notifications daemon, etc. And for issuing commands from terminal.

* Data model
** MQTT topics
There are two types of messages we expect and they will arrive on the following topics:
- Sensor data (~sensor~):
    - ~michurin/$SENSOR_TOPIC/$CLIENT_ID/$PLANT_ID~
- Commands (~command~):
    - ~michurin/$COMMAND_TOPIC/$CLIENT_ID/$PLANT_ID~

* References

https://stackoverflow.com/questions/31156817/how-to-set-username-password-mosquitto

* Tasks
** DONE Rethink the payload structure and message flow
CLOSED: [2022-09-20 Tue 19:44]
Both data payloads and commands should be accomodated.
Should we just subscribe to everything, and then sort the messages based on topics? And handle them conditionally on the message type?
** TODO Save firmware state (calibration) in EEPROM
