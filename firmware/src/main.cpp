#include <SPI.h>
#include <WiFi101.h>
#include <MQTT.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include "NTP.h"
// #include <EEPROM.h>
// https://docs.arduino.cc/learn/built-in-libraries/eeprom

// Wifi and MQTT configuration.
#include "secrets.h"
const char wifi_ssid[] = WIFI_SSID;
const char wifi_pass[] = WIFI_PASS;
const char broker_url[] = MQTT_URL;
const char broker_user[] = MQTT_USER;
const char broker_pass[] = MQTT_PASS;
const char base_topic[] = BASE_TOPIC;
const char sensor_topic[] = SENSOR_TOPIC;
const char command_topic[] = COMMAND_TOPIC;
const char client_id[] = CLIENT_ID;
const char ingestion_target_id[] = INGESTION_TARGET_ID;
const char plant_id[] = PLANT_ID;
// Default calibration values for the moisture sensor
// based on empirical measurement.
const float zeroAir = 890.0;
const float zeroWater = 300.0;
float moistureSpan = zeroAir - zeroWater;
// EEPROM.update(address, value)
// Measurands.
float moistureValue;
float moistureRelative;
// Time counter for the main loop sleep.
unsigned long lastMillis = 0;
// Loop length in milliseconds.
const int loopSleepInterval = 30000;
// Wifi driver
WiFiClient wifiClient;
WiFiUDP wifiUdp;
// WiFi radio's status.
int status = WL_IDLE_STATUS;
// Network time.
NTP ntp(wifiUdp);
// MQTT client.
MQTTClient mqttClient(1024);


void printMacAddress(byte mac[]) {
	for (int i = 5; i >= 0; i--) {
		if (mac[i] < 16) {
			Serial.print("0");
		}
		Serial.print(mac[i], HEX);
		if (i > 0) {
			Serial.print(":");
		}
	}
	Serial.println();
}

void printWiFiData() {
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);
	byte mac[6];
	WiFi.macAddress(mac);
	Serial.print("MAC address: ");
	printMacAddress(mac);
}

void printCurrentNet() {
	// Print the SSID of the connected Wifi network
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());
	// Print the MAC address of the router
	byte bssid[6];
	WiFi.BSSID(bssid);
	Serial.print("BSSID: ");
	printMacAddress(bssid);
	// Print the RSSI
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.println(rssi);
	// Print the encryption type
	byte encryption = WiFi.encryptionType();
	Serial.print("Encryption Type:");
	Serial.println(encryption, HEX);
	Serial.println();
}

void connectWifi() {
	// Attempt to connect to the WiFi network
	while (status != WL_CONNECTED) {
		Serial.print("Attempting to connect to WPA SSID: ");
		Serial.println(wifi_ssid);
		// Connect to WPA/WPA2 network
		status = WiFi.begin(wifi_ssid, wifi_pass);
		delay(5000);
	}
	printCurrentNet();
	printWiFiData();
}

void connectMQTT() {
	// Connect to the MQTT broker and subscribe
	// to the command topic (listen for commands).
	Serial.print("Attempting to connect to MQTT: ");
	Serial.println(broker_url);
	Serial.print("MQTT U:P: ");
	Serial.println(String(broker_user) + ":" + String(broker_pass));

	while (!mqttClient.connected()) {
		if (mqttClient.connect(client_id, broker_user, broker_pass)) {
			Serial.println("\nMQTT connected!");
			// Only listen to commands directed to this client.
			String topic = String(base_topic)
				+ "/" + String(command_topic)
				+ "/" + String(client_id)
				+ "/#";
			Serial.print("Subscribing to topic: ");
			Serial.println(topic);
			mqttClient.subscribe(topic);
		} else {
			Serial.print(".");
			delay(1000);
		}
	}
}

float measureMoisture() {
	// Read raw moisture value off the connected sensor.
	float moistureValue = float(analogRead(A1));
	return moistureValue;
}

float relativeMoisture(float moistureValue) {
	// Calculate the relative moisture based on the raw sensor
	// value and the calibration config. If well-calibrated,
	// this should be a number between 0 (air) and 1 (water).
	float moistureRelative = 1.0 - ((moistureValue - zeroWater) / moistureSpan);
	return moistureRelative;
}

void pumpWater(int duration) {
	// Activate the water pump for a duration in milliseconds.
	Serial.println("Pumping water for " + String(duration) + " ms.");
	digitalWrite(1, LOW);
	delay(duration);
	digitalWrite(1, HIGH);
	Serial.println("Pumping finished!");
}

void messageReceived(String &topic, String &payload) {
	// Handle incoming messages (most likely commands from the server).
	Serial.println("#### INCOMING MESSAGE ####");
	Serial.println(topic + "-" + payload);

	DynamicJsonDocument doc(1024);
	deserializeJson(doc, payload);
	const String mType = doc["mType"];
	const String cmdName = doc["data"][0]["name"];
	const int cmdValue = doc["data"][0]["value"];

	Serial.println(mType);
	Serial.println(cmdName);
	Serial.println(cmdValue);

	if (mType == "command" && cmdName == "water") {
		pumpWater(1000 * cmdValue);
	}
}

void measureAndSubmit(int online) {
	// Make a measurement, form a payload, and submit it
	// to the data hub (MQTT broker).
	// If network is not available, crucial decisions
	// (like to water or not) will be made here offline.
	//
	// TODO: Implement the online/offline logic.
	// If offline, watering decision will be made locally,
	// and no payload will be submitted.
	//
	Serial.println("\n-------------------------------------");
	// Update the network time.
	ntp.update();
	Serial.println(ntp.formattedTime("%Y-%m-%dT%H:%M:%SZ"));

	// Measure moisture.
	float moistureAbsolute = measureMoisture();
	Serial.print("Absolute moisture level: ");
	Serial.println(moistureAbsolute);
	float moistureRelative = relativeMoisture(moistureAbsolute);
	Serial.print("Relative moisture level: ");
	Serial.println(moistureRelative);

	// Running offline:
	// if (moistureRelative < 0.25) {
	// 	pumpWater(4000);
	// 	Serial.println();
	// }

	// Form data payload to be submitted.
	DynamicJsonDocument payload(1024);
	payload["timestamp"] = ntp.formattedTime("%Y-%m-%dT%H:%M:%SZ");
	payload["senderId"] = client_id;
	payload["targetId"] = ingestion_target_id;
	payload["plantId"] = plant_id;
	payload["mType"] = sensor_topic;
	payload["data"][0]["name"] = "moisture";
	payload["data"][0]["value"] = moistureRelative;
	payload["data"][1]["name"] = "moistureAbsolute";
	payload["data"][1]["value"] = moistureAbsolute;
	payload["data"][2]["name"] = "zeroAir";
	payload["data"][2]["value"] = zeroAir;
	payload["data"][3]["name"] = "zeroWater";
	payload["data"][3]["value"] = zeroWater;

	// Serialize the payload.
	String payloadStr;
	serializeJson(payload, payloadStr);
	Serial.println("Submitting payload.");
	Serial.println(payloadStr);

	// Publish the payload.
	String topic = String(base_topic)
			+ "/" + String(sensor_topic)
			+ "/" + String(client_id)
			+ "/" + String(plant_id);
	Serial.println(topic);

	Serial.println("Attempting to publish a message.");
	if (mqttClient.publish(topic, payloadStr)) {
		Serial.println("Success.");
	} else {
		Serial.println("Failed!");
	}
}

void setup() {
	// Initialize serial and wait for port to open
	Serial.begin(9600);
	delay(3000);
	// Set the water pump pin (1) to OUTPUT and switch it off
	pinMode(1, OUTPUT);
	digitalWrite(1, HIGH);
	// Setup Wifi
	connectWifi();
	// Setup NTP clock
	ntp.begin();
	// Setup MQTT
	mqttClient.begin(broker_url, wifiClient);
	mqttClient.onMessage(messageReceived);
	connectMQTT();
	delay(1000);
}

void loop() {
	// Main loop.
	//
	// Make sure everything is connected.
	if (status != WL_CONNECTED) {
		connectWifi();
		Serial.println();
	}
	if (!mqttClient.connected()) {
		connectMQTT();
		Serial.println();
	}
	// Loop async sleep. With sync delay-based sleep there is
	// a risk of missing incoming MQTT events.
	if (millis() - lastMillis > loopSleepInterval) {
		lastMillis = millis();
		// set to 0 if wifi/mqtt connection cannot be established.
		int online = 1;
		measureAndSubmit(online);
	}
	// MQTT loop (receive and send messages).
	if (!mqttClient.loop()) {
		Serial.println("MQTT error!");
		Serial.println(mqttClient.returnCode());
		Serial.println(mqttClient.lastError());
	}
}
