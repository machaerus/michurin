#!/usr/bin/env python
"""
Simple script to test sending MQTT commands from
a desktop device to the plant.
"""

from datetime import datetime as dt
import json
import os

from paho.mqtt import client as mqtt_client
from dotenv import load_dotenv
load_dotenv("../ingestion/production.env")
load_dotenv("./config")

BROKER_USER = os.getenv("BROKER_USER")
BROKER_PASS = os.getenv("BROKER_PASS")
BROKER_URL = os.getenv("MQTT_HOST")
TARGET_ID = os.getenv("TARGET_ID")
PLANT_ID = os.getenv("PLANT_ID")


def connect_mqtt():
	"""
	Connect and authenticate to the MQTT server.

	Returns
	-------
	client : mqtt_client
	"""
	def on_connect(client, userdata, flags, rc):
		"""On-connect callback, not really needed here."""
		if rc == 0:
			print("Connected to MQTT Broker!")
		else:
			print("Failed to connect, return code %d\n", rc)
	# Set Connecting Client ID
	client = mqtt_client.Client(os.getlogin())
	client.username_pw_set(BROKER_USER, BROKER_PASS)
	client.on_connect = on_connect
	client.connect(BROKER_URL, 1883)
	return client


def water(client, secs):
	"""
	Send a water command to water the plant
	for a given number of seconds.
	"""
	timestamp = dt.now().isoformat()[:-3] + "Z"
	payload = {
		"timestamp": timestamp,
		"senderId": os.getlogin(),
		"targetId": TARGET_ID,
		"plantId": PLANT_ID,
		"mType": "command",
		"data": [
			{
				"name": "water",
				"value": secs
			}
		]
	}
	topic = f"michurin/command/{TARGET_ID}/{PLANT_ID}"
	print("Sending payload")
	result = client.publish(topic, json.dumps(payload))
	# result: [0, 1]
	status = result[0]
	if status == 0:
		print("Message sent successfully!")
	else:
		print("Failed to send the message!")


if __name__ == "__main__":

	print(BROKER_URL)
	print(BROKER_USER)
	print(BROKER_PASS)

	print("Creating client")
	c = connect_mqtt()
	water(c, 3)
